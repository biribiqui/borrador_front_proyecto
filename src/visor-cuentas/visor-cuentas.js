import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all: initial;*/
          border: solid blue;
        }

      </style>

      <h2>IBAN [[iban]]</h2>
      <h2>el saldo es [[balance]]</h2>

      <iron-ajax
        auto
        id="getUser"
        url="http://localhost:3000/apitechu/v2/accounts/{{id}}"
        handle-as="json"
        on-response="showData"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      iban: {
        type: String
      }, id: {
        type: Number
      }, balance: {
        type: Number
      }
    };
  }   //end properties

  showData(data) {
    console.log("showData");

    console.log(data.detail.response[0]);
    console.log(data.detail.response[0].iban);
    console.log(data.detail.response[0].idusuario);
    console.log(data.detail.response[0].balance);
    console.log("fin showdata");

    this.iban = data.detail.response[0].iban;
    this.id = data.detail.response[0].idusuario;
    this.balance = data.detail.response[0].balance;
  }
}  //end class

window.customElements.define('visor-cuentas', VisorCuentas);

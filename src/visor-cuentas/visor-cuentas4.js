import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {} from '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all: initial;*/
        }

      </style>

      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <br/>

      <div class="container">
  			<table class="table">
          <thead class="thead-light">
          <tr>
            <th>IBAN</th>
            <th>SALDO</th>
          </tr>
        </thead>
      	<tbody>
<!--          <dom-repeat items="{{cuentas}}"> -->
          <template is="dom-repeat" items="{{cuentas}}">
      	     <tr>
				         <td>[[item.iban]]</td>
						     <td>[[item.balance]]</td>
      		   </tr>
          </template>
<!--          </dom-repeat> -->
      	</tbody>
        </table>
      </div>

      <iron-ajax
        id="getUser"
        url="http://localhost:3000/apitechu/v2/accounts/{{idUser}}"
        handle-as="json"
        on-response="showData"
        on-error="trataError"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      cuentas: {
        type: Array
      }, idUser: {
        type: Number,
        observer: "_idUserChanged"
      }
    };
  }   //end properties

  _idUserChanged(newValue, oldValue) {
     console.log("Id value has changed");
     console.log("new value is " + newValue);
     console.log("old value is " + oldValue);

     this.$.getUser.generateRequest();
   }

  showData(data) {
    console.log("showData");

    console.log(data.detail.response[0]);
    console.log(data.detail.response[0].iban);
    console.log(data.detail.response[0].idusuario);
    console.log(data.detail.response[0].balance);
    console.log("fin showdata");

//    this.iban = data.detail.response[0].iban;
//    this.id = data.detail.response[0].idusuario;
//    this.balance = data.detail.response[0].balance;
    this.cuentas = data.detail.response;
    console.log("prueba");
    console.log(this.cuentas);
    console.log("fin prueba");
  }

  trataError(error) {
    console.log("Hubo un error");
    console.log(error);
    console.log(error.detail.error.message);

    this.cuentas = [];
  }
}  //end class

window.customElements.define('visor-cuentas4', VisorCuentas);

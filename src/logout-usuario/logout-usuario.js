import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class LogoutUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos V1.css" rel="stylesheet" type="text/css" />

      <div id="blocker">
      </div>

      <div class="card card-logout" id="login">
    		<h4 class="card-title">LOG OUT</h4>
    		</br>
        <div>¿Está seguro de que desea desconectar?
        </div>
    		</br>
        <div id="cajaerror" class="alertaerror">
          <div class="row">
            <div class="offset-4">ERROR:</div>
          </div>
          </br>
          <div class="row">
            <div id="errorlogin">AQUI EL MENSAJE DE ERROR</div>
          </div>
        </div>
    		</br>
    		<div class="text-center">
    			<button type="button" class="btn btn-secondary btn-formulario" id="btnAceptar" on-click="logoutUsuario">Aceptar</button>
    			<button type="button" class="btn btn-secondary btn-formulario" id="btnCancelar" on-click="cancelaLogout">Cancelar</button>
    		</div>
    	</div>

      <iron-ajax
        id="doLogout"
        url="http://localhost:3000/apitechu/v2/logout/{{idUsuario}}"
        handle-as="json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      idUsuario: {
        type: Number
      }
    };
  }

  logoutUsuario() {
    console.log("Log Out pulsado " + this.idUsuario);

    this.$.doLogout.generateRequest();
  }

  cancelaLogout() {
    console.log("Cancelar pulsado");

    var elemento = this.$.cajaerror;

    elemento.style.color = "#9F2B57";

    this.generaEvento("cancelaLogOut");
  }

  manageAJAXresponse(data) {

    if (data.detail.response.msg == "Logout correcto" || "Error usuario no logado") {

        this.idUsuario = null;

        this.generaEvento("logOut");
    }
    else {
      var elemento = this.$.errorlogin;

      elemento.textContent = data.detail.response.msg;

      elemento = this.$.cajaerror;

      elemento.style.color = "black";
    }
  }

  showError(error) {

    var elemento = this.$.errorlogin;

    elemento.textContent = error.detail.error.message;

    elemento = this.$.cajaerror;

    elemento.style.color = "black";
  }

  generaEvento(evento) {

    this.dispatchEvent(
      new CustomEvent(
        "eventlogout",
        {
          detail: {
            "evento": evento
          }
        }
      )
    )
  }
}

window.customElements.define('logout-usuario', LogoutUsuario);

import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos V1.css" rel="stylesheet" type="text/css" />

      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/apitechu/v2/login"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      email: {
        type: String
      }, password: {
        type: String
      }, isLogged: {
        type: Boolean,
        value: false
      }
    };
  }

  Login() {
    console.log("El usuario ha pulsado el boton");

    var loginData = {
      "email": this.email,
      "password": this.password
    }

//    console.log(loginData);

    this.$.doLogin.body = JSON.stringify(loginData);
    this.$.doLogin.generateRequest();
  }

  manageAJAXresponse(data) {
    console.log("Llegaron los resultados");
    console.log(data.detail.response);

    this.isLogged = true;
  }

  showError(error) {
    console.log("Hubo un error");
    console.log(error);
  }
}

window.customElements.define('login-usuario', LoginUsuario);

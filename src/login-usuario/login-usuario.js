import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos V1.css" rel="stylesheet" type="text/css" />

      <div id="blocker">
      </div>

      <div class="card card-login" id="login">
    		<h4 class="card-title">LOG IN</h4>
    		</br>
    		<form name="datoslogin" action="#">
    			<div class="form-group">
    				<label>Correo Electrónico</label>
    					<input id="email" type="email" class="form-control"  value="{{email::input}}" on-change="validaCompleto" autofocus>
    			</div>
    			<div class="form-group">
    				<label>Contraseña</label>
    				<input id="password" type="password" class="form-control" value="{{password::input}}" on-change="validaCompleto">
    			</div>
    		</form>
    		</br>
        <div id="cajaerror" class="alertaerror">
            <span id="errorlogin">ERROR:</span>
            </br>
        </div>
    		</br>
    		</br>
    		<div class="text-center">
    			<button type="button" class="btn btn-secondary btn-formulario" id="btnLogin" on-click="loginUsuario" disabled>Log In</button>
    			<button type="button" class="btn btn-secondary btn-formulario" id="btnLogCancel" on-click="cancelaLogIn">Cancelar</button>
    		</div>
    	</div>

      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/apitechu/v2/login"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      email: {
        type: String
      }, password: {
        type: String
      }, idUsuario: {
        type: Number
      }, name: {
        type: String
      }
    };
  }

  loginUsuario() {
    console.log("Log In pulsado");

    var loginData = {
      "email": this.email,
      "password": this.password
    }

    this.$.doLogin.body = JSON.stringify(loginData);
    this.$.doLogin.generateRequest();
  }

  cancelaLogIn() {
    console.log("Cancelar pulsado");

    this.inicializa();

    this.generaEvento("cancelaLogIn");
  }

  manageAJAXresponse(data) {

    if (data.detail.response.msg == "Login correcto") {

        this.inicializa();

        this.idUsuario = data.detail.response.idUsuario;
        this.name = data.detail.response.first_name;

        this.generaEvento("logIn");
    }
    else {
      var elemento = this.$.errorlogin;

      elemento.textContent = "ERROR: " + data.detail.response.msg;

      elemento = this.$.cajaerror;

      elemento.style.color = "black";
    }
  }

  showError(error) {

    var elemento = this.$.errorlogin;

    if (error.detail.request.status == 404){

      elemento.textContent = "ERROR: datos incorrectos";
    }
    else {
      elemento.textContent = "ERROR: " + error.detail.error.message;
    }

    elemento = this.$.cajaerror;

    elemento.style.color = "black";
  }

  generaEvento(evento) {

    this.dispatchEvent(
      new CustomEvent(
        "eventlogin",
        {
          detail: {
            "evento": evento,
            "idUsuario" : this.idUsuario,
            "name": this.name
          }
        }
      )
    )
  }

  validaCompleto() {

    var elemento1 = this.$.email;
    var elemento2 = this.$.password;

    if (elemento1.value.length > 0 && elemento2.value.length > 0){

      var elemento3 = this.$.btnLogin;

      elemento3.disabled = false;
      elemento3.focus();
    }
  }

  inicializa() {

    var elemento = this.$.cajaerror;

    elemento.style.color = "#9F2B57";

    elemento = this.$.btnLogin;

    elemento.disabled = true;

    elemento = this.$.email;

    elemento.focus();

    this.email = "";
    this.password = "";
  }
}

window.customElements.define('login-usuario', LoginUsuario);

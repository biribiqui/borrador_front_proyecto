import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>


      <input type="email" placeholder="email" value="{{email::input}}"/>
      <br/>
      <input type="password" placeholder="password" value="{{password::input}}"/>
      <br/>
      <button on-click="Login">Login</button>
      <span hidden$="[[!isLogged]]">Bienvenido/a de nuevo</span>

      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/apitechu/v2/login"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      email: {
        type: String
      }, password: {
        type: String
      }, isLogged: {
        type: Boolean,
        value: false
      }
    };
  }

  Login() {
    console.log("El usuario ha pulsado el boton");

    var loginData = {
      "email": this.email,
      "password": this.password
    }

//    console.log(loginData);

    this.$.doLogin.body = JSON.stringify(loginData);
    this.$.doLogin.generateRequest();
  }

  manageAJAXresponse(data) {
    console.log("Llegaron los resultados");
    console.log(data.detail.response);
    console.log(data.detail.response.idUsuario);

    this.isLogged = true;

    console.log("Logado " + this.isLogged);

    this.dispatchEvent(
      new CustomEvent(
        "myevent",
        {
          detail: {
            "idUser": data.detail.response.idUsuario
          }
        }
      )
    )
  }

  showError(error) {
    console.log("Hubo un error");
    console.log(error);
  }
}

window.customElements.define('login-usuario2', LoginUsuario);

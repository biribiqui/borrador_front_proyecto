import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../login-usuario/login-usuario2.js';
import '../visor-usuario/visor-usuario2.js';
import "@polymer/iron-pages/iron-pages.js"

/**
 * @customElement
 * @polymer
 */
class UserDashboard extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <h3>User Dashboard</h3>
      <iron-pages selected="[[componentname]]" attr-for-selected="component-name">
        <div component-name="login-usuario"><login-usuario2 on-myevent="processEvent"></login-usuario2></div>
        <div component-name="visor-usuario"><visor-usuario2 id="visorusuario" componente="[[componentname]]"></visor-usuario2></div>
      </iron-pages>

<!--      <login-usuario2 on-myevent="processEvent"></login-usuario2> -->
<!--      <visor-usuario2 id="visorusuario"></visor-usuario2> -->

    `;
  }

  static get properties() {
    return {
      componentname: {
        type: String
      }
    };
  }

  processEvent(e) {
    console.log("Capturado evento del emisor");
    console.log(e);

//    this.$.receiver.id = e.detail.idUser;

    console.log(e.detail.idUser);

    this.componentname = "visor-usuario";
    this.$.visorusuario.idUser = e.detail.idUser;
  }

}

window.customElements.define('user-dashboard', UserDashboard);

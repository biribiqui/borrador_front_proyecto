import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../login-usuario/login-usuario.js';
import '../logout-usuario/logout-usuario.js';
import '../alta-usuario/alta-usuario.js';
import "@polymer/iron-pages/iron-pages.js"

/**
 * @customElement
 * @polymer
 */
class PaginaPrincipal extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          background-color: #E72085;
          width: 100%;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos V1.css" rel="stylesheet" type="text/css" />

      <div class="card" id="top">
    		<div class="container-fluid">
    			<div class="row">
    				<div class="col-1 offset-2" id="bandera">
    					<img src="src/pagina-principal/fotos/bandera-moderdonia-pequeña-2.jpg">
    				</div>
    				<div class="col-9">
    					<div class="col-12">
    						<div class="row">
    							<div class="col-1 offset-3" id="logo">
    								<img src="src/pagina-principal/fotos/logo.png">
    							</div>
    							<div class="col-5 offset-3 btn-inicio">
    								<button type="button" id="btnalta" class="btn btn-secondary btn-alb" on-click="altaUsuario">Registrarse</button>
    								<button type="button" id="btnlogin" class="btn btn-secondary btn-alb" on-click="logInlogOut">Log In</button>
    								<button type="button" id="btnusuario" class="btn btn-secondary dropdown-toggle btn-alb" data-toggle="dropdown"  hidden></button>
    								<div class="dropdown-menu dropdown-alb">
    									<a class="dropdown-item dropdown-alb" href="#">Editar</a>
    								</div>
    							</div>
    						</div>
    					</div>

    					<div class="col-12">
    						<nav class="navbar navbar-expand-sm navbar-light">
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">PARTICULARES</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">BANCA PERSONAL</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">BANCA PRIVADA</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">AUTONOMOS</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">EMPRESAS</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">INSTITUCIONES</a>
    								</li>
    							</ul>
    						</nav>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>

    	<div class="card" id="menu">
    		<div class="container">
    			<div class="row">
    				<nav class="navbar navbar-expand navbar-dark offset-1">
    					<ul class="navbar-nav">
    						<div class="card card-icon">
    							<img src="src/pagina-principal/fotos/icons8-casa-24.png">
    						</div>
    						<li class="nav-item alb-item2">
    							<a class="nav-link">Home</a>
    						</li>
    						<div class="card card-icon">
    							<img src="src/pagina-principal/fotos/icons8-montón-de-dinero-24.png">
    						</div>
    						<li class="nav-item alb-item2">
    							<a class="nav-link">Cuentas</a>
    						</li>
    						<div class="card card-icon">
    							<img src="src/pagina-principal/fotos/icons8-tarjetas-bancarias-filled-24.png">
    						</div>
    						<li class="nav-item alb-item2">
    							<a class="nav-link">Tarjetas</a>
    						</li>
    						<div class="card card-icon">
    							<img src="src/pagina-principal/fotos/icons8-hipoteca-filled-24.png">
    						</div>
    						<li class="nav-item dropdown alb-item2">
    							<a class="nav-link dropdown-toggle" data-toggle="dropdown">Hipotecas y Préstamos</a>
    								<div class="dropdown-menu dropdown-alb">
    									<a class="dropdown-item dropdown-alb">Calcule su Préstamo</a>
    								</div>
    						</li>
    						<li class="nav-item alb-item2">
    						</li>
    						<div class="card card-icon">
    							<img src="src/pagina-principal/fotos/icons8-acerca-de-24.png">
    						</div>
    						<li class="nav-item dropdown alb-item2">
    							<a class="nav-link dropdown-toggle" data-toggle="dropdown">About</a>
    								<div class="dropdown-menu dropdown-alb">
    									<a class="dropdown-item dropdown-alb">Quienes Somos</a>
    									<a class="dropdown-item dropdown-alb">Donde Estamos</a>
    									<a class="dropdown-item dropdown-alb">Nuestro Equipo</a>
    									<a class="dropdown-item dropdown-alb">Contacto</a>
    								</div>
    						</li>
    					</ul>
    				</nav>
    			</div>
    		</div>
    	</div>

      <div class="container">
		    <img class="img-fluid mx-auto" src="src/pagina-principal/fotos/moderdonia2-930x600.jpg" width="100%" height="100%">
      </div>

      <iron-pages selected="[[componente]]" attr-for-selected="component-name">
        <div component-name="login-usuario"><login-usuario on-eventlogin="procesaEvento"></login-usuario></div>
        <div component-name="alta-usuario"><alta-usuario on-eventalta="procesaEvento"></alta-usuario></div>
        <div component-name="logout-usuario"><logout-usuario id="logout" on-eventlogout="procesaEvento"></logout-usuario></div>
      </iron-pages>

    `;
  }

  static get properties() {
    return {
      componente: {
        type: String
      }, idUsuario: {
        type: Number
      }, logged: {
        type: Boolean,
        value: false
      }
    };
  }

  logInlogOut() {

    if (!this.logged){

      this.componente="login-usuario";
    }
    else {
      this.$.logout.idUsuario = this.idUsuario;
      this.componente="logout-usuario";
    }
  }

  altaUsuario() {

      this.componente="alta-usuario";
  }

  procesaEvento(e) {
    console.log("Capturado evento del emisor");
    console.log(e.detail.evento);

    switch (e.detail.evento)
		{
	    case "logIn":

        this.usuarioConectado(e.detail.idUsuario, e.detail.name);

        break;

      case "logOut":

        this.usuarioDesconectado();

        break;

      case "alta":

        this.usuarioConectado(e.detail.idUsuario, e.detail.name);

        break;

      default:

       this.componente="pagina-principal";
    }
  }

  usuarioConectado(usuario, nombre) {

    console.log("logando " + usuario + " " + nombre);
    var elemento = this.$.btnlogin;

    elemento.textContent = "Log Out";

    elemento = this.$.btnusuario;

    elemento.textContent = nombre;
    elemento.hidden = false;

    elemento = this.$.btnalta;
    elemento.disabled = true;

    this.logged = true;
    this.idUsuario = usuario;
    this.componente="pagina-principal";
  }

  usuarioDesconectado() {

    var elemento = this.$.btnlogin;

    elemento.textContent = "Log In";

    elemento = this.$.btnusuario;

    elemento.textContent = "";
    elemento.hidden = true;

    elemento = this.$.btnalta;
    elemento.disabled = false;

    this.logged = false;
    this.idUsuario = null;
    this.componente="pagina-principal";
  }
}

window.customElements.define('pagina-principal', PaginaPrincipal);

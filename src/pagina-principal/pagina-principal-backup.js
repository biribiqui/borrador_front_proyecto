import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * @customElement
 * @polymer
 */
class PaginaPrincipal extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos V1.css" rel="stylesheet" type="text/css" />

      <div class="card" id="top">
    		<div class="container-fluid">
    			<div class="row">
    				<div class="col-1 offset-2" id="bandera">
    					<img src="src/pagina-principal/fotos/bandera-moderdonia-pequeña-2.jpg">
    				</div>
    				<div class="col-9">
    					<div class="col-12">
    						<div class="row">
    							<div class="col-1 offset-3" id="logo">
    								<img src="src/pagina-principal/fotos/logo.png">
    							</div>
    							<div class="col-5 offset-3 btn-inicio">
    								<button type="button" class="btn btn-secondary btn-alb">Registrarse</button>
    								<button type="button" class="btn btn-secondary btn-alb" on-click="generaEventoLogIn">Log In</button>
    								<button type="button" class="btn btn-secondary dropdown-toggle btn-alb" data-toggle="dropdown">Alberto</button>
    								<div class="dropdown-menu dropdown-alb">
    									<a class="dropdown-item dropdown-alb" href="#">Editar</a>
    								</div>
    							</div>
    						</div>
    					</div>

    					<div class="col-12">
    						<nav class="navbar navbar-expand-sm navbar-light">
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">PARTICULARES</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">BANCA PERSONAL</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">BANCA PRIVADA</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">AUTONOMOS</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">EMPRESAS</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">INSTITUCIONES</a>
    								</li>
    							</ul>
    						</nav>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>

    	<div class="card" id="menu">
    		<div class="container">
    			<div class="row">
    				<nav class="navbar navbar-expand navbar-dark offset-1">
    					<ul class="navbar-nav">
    						<div class="card card-icon">
    							<img src="src/pagina-principal/fotos/icons8-casa-24.png">
    						</div>
    						<li class="nav-item alb-item2">
    							<a class="nav-link" href="#">Home</a>
    						</li>
    						<div class="card card-icon">
    							<img src="src/pagina-principal/fotos/icons8-montón-de-dinero-24.png">
    						</div>
    						<li class="nav-item alb-item2">
    							<a class="nav-link" href="#">Cuentas</a>
    						</li>
    						<div class="card card-icon">
    							<img src="src/pagina-principal/fotos/icons8-tarjetas-bancarias-filled-24.png">
    						</div>
    						<li class="nav-item alb-item2">
    							<a class="nav-link" href="#">Tarjetas</a>
    						</li>
    						<div class="card card-icon">
    							<img src="src/pagina-principal/fotos/icons8-hipoteca-filled-24.png">
    						</div>
    						<li class="nav-item dropdown alb-item2">
    							<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Hipotecas y Préstamos</a>
    								<div class="dropdown-menu dropdown-alb">
    									<a class="dropdown-item dropdown-alb" href="#">Calcule su Préstamo</a>
    								</div>
    						</li>
    						<li class="nav-item alb-item2">
    						</li>
    						<div class="card card-icon">
    							<img src="src/pagina-principal/fotos/icons8-acerca-de-24.png">
    						</div>
    						<li class="nav-item dropdown alb-item2">
    							<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">About</a>
    								<div class="dropdown-menu dropdown-alb">
    									<a class="dropdown-item dropdown-alb" href="#">Quienes Somos</a>
    									<a class="dropdown-item dropdown-alb" href="#">Donde Estamos</a>
    									<a class="dropdown-item dropdown-alb" href="#">Nuestro Equipo</a>
    									<a class="dropdown-item dropdown-alb" href="#">Contacto</a>
    								</div>
    						</li>
    					</ul>
    				</nav>
    			</div>
    		</div>
    	</div>

    	<div class="container">
    		<div id="demo" class="carousel slide" data-ride="carousel">

    		<!-- Indicators -->
    			<ul class="carousel-indicators">
    				<li data-target="#demo" data-slide-to="0" class="active"></li>
    				<li data-target="#demo" data-slide-to="1"></li>
    				<li data-target="#demo" data-slide-to="2"></li>
    			</ul>

    		<!-- The slideshow -->
    			<div class="carousel-inner">
    				<div class="carousel-item active">
    					<img src="src/pagina-principal/fotos/moderdonia2-744x480.jpg" alt="foto1" style="width:100%;">
    				</div>
    				<div class="carousel-item">
    					<img src="src/pagina-principal/fotos/vida-moderna-25518-2.jpg" alt="foto2" style="width:100%;">
    				</div>
    				<div class="carousel-item">
    					<img src="src/pagina-principal/fotos/DChYfmIXYAEXX1-.jpg" alt="foto3" style="width:100%;">
    				</div>
    			</div>

    		<!-- Left and right controls -->
    			<a class="carousel-control-prev" href="#demo" data-slide="prev">
    				<span class="carousel-control-prev-icon"></span>
    			</a>
    			<a class="carousel-control-next" href="#demo" data-slide="next">
    				<span class="carousel-control-next-icon"></span>
    			</a>
    		</div>
    	</div>

    					<div class="col-1">
    					<audio src="Ipno de Moderdonia.mp3" controls>
    					</audio>
    				</div>


    `;
  }

  static get properties() {
    return {

    };
  }

  generaEventoLogIn(e) {
    console.log("Log In pulsado");

    this.dispatchEvent(
      new CustomEvent(
        "myevent",
        {
          detail: {
            "evento": "activaLogIn",
          }
        }
      )
    )
  }
}

window.customElements.define('pagina-principal', PaginaPrincipal);

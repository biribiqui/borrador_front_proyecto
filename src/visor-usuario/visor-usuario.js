import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all: initial;*/
          border: solid blue;
        }

        .redbg {
          background-color: red;
        }

        .greenbg {
          background-color: green;
        }

        .bluebg {
          background-color: blue;
        }

        .greybg {
          background-color: grey;
        }
      </style>

      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <div class="row greybg">
        <div class="col-2 offset-1 redbg">COL 1</div>
        <div class="col-3 offset-2 greenbg">COL 2</div>
        <div class="col-4 bluebg">COL 3</div>
      </div>

      <button class="btn btn-info">Login</button>
      <button class="btn btn-success">Logout</button>

      <h2>Soy [[first_name]] [[last_name]]</h2>
      <h2>mi email es [[email]]</h2>

      <iron-ajax
        auto
        id="getUser"
        url="http://localhost:3000/apitechu/v2/users/{{id}}"
        handle-as="json"
        on-response="showData"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      }, last_name: {
        type: String
      }, email: {
        type: String
      }, id: {
        type: Number
      }
    };
  }   //end properties

  showData(data) {
    console.log("showData");

    console.log(data.detail.response);
    console.log(data.detail.response.first_name);

    this.first_name = data.detail.response.first_name;
    this.last_name = data.detail.response.last_name;
    this.email = data.detail.response.email;

  }
}  //end class

window.customElements.define('visor-usuario', VisorUsuario);

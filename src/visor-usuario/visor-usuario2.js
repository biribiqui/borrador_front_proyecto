import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import "@polymer/iron-pages/iron-pages.js"
import '../visor-cuentas/visor-cuentas4.js';

/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all: initial;*/
          border: solid blue;
        }

        .redbg {
          background-color: red;
        }

        .greenbg {
          background-color: green;
        }

        .bluebg {
          background-color: blue;
        }

        .greybg {
          background-color: grey;
        }
      </style>

      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <div class="row greybg">
        <div class="col-2 offset-1 redbg">COL 1</div>
        <div class="col-3 offset-2 greenbg">COL 2</div>
        <div class="col-4 bluebg">COL 3</div>
      </div>

      <button class="btn btn-info">Login</button>
      <button class="btn btn-success">Logout</button>

      <h2>Soy [[first_name]] [[last_name]]</h2>
      <h2>mi email es [[email]]</h2>
      <button on-click="Consulta">Consultar Cuentas</button>

      <iron-pages selected="[[componente]]" attr-for-selected="component-name">
        <div component-name="visor-cuentas"><visor-cuentas4 id="visorcuentas"></visor-cuentas4></div>
      </iron-pages>


      <iron-ajax
        id="getUser"
        url="http://localhost:3000/apitechu/v2/users/{{idUser}}"
        handle-as="json"
        on-response="showData"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      first_name: {
        type: String
      }, last_name: {
        type: String
      }, email: {
        type: String
      }, idUser: {
        type: Number,
        observer: "_idUserChanged"
      }, componente: {
        type: String
      }
    };
  }   //end properties

  _idUserChanged(newValue, oldValue) {
     console.log("Id value has changed");
     console.log("new value is " + newValue);
     console.log("old value is " + oldValue);

     this.$.getUser.generateRequest();
   }

  showData(data) {
    console.log("showData");

    console.log(data.detail.response);
    console.log(data.detail.response.first_name);

    this.first_name = data.detail.response.first_name;
    this.last_name = data.detail.response.last_name;
    this.email = data.detail.response.e_mail;

    this.componentname = "visor-cuentas"
    this.$.visorcuentas.idUser = this.idUser;

  }

  Consulta() {
    console.log("El usuario ha pulsado el boton");

    this.componente = "visor-cuentas";
  }

}  //end class

window.customElements.define('visor-usuario2', VisorUsuario);
